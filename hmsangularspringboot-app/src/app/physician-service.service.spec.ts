import { TestBed } from '@angular/core/testing';

import { PhysicianServiceService } from './physician-service.service';

describe('PhysicianServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PhysicianServiceService = TestBed.get(PhysicianServiceService);
    expect(service).toBeTruthy();
  });
});
