export class Physician {
    id:number;
    firstName:string;
    lastName:string;
    department:string;
    qualification:string;
    yearsOfExperience:number;
}
