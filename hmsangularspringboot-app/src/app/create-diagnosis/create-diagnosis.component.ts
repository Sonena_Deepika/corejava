import { Component, OnInit } from '@angular/core';
import { Diagnosis } from '../diagnosis';
import { DiagnosisServiceService } from '../diagnosis-service.service';

@Component({
  selector: 'app-create-diagnosis',
  templateUrl: './create-diagnosis.component.html',
  styleUrls: ['./create-diagnosis.component.css']
})
export class CreateDiagnosisComponent implements OnInit {

  
  diagnosis:Diagnosis = new Diagnosis();
  submitted=false;
  constructor(private diagnosisService:DiagnosisServiceService) { 

  }

  ngOnInit() {
  }

  onSubmit(){
    console.log("successfully submitted");
    this.save();
  }

  save() {
    this.diagnosisService.createDiagnosis(this.diagnosis)
      .subscribe(
        data => {
          console.log(data);
          this.submitted = true;
        },
        error => console.log(error));
    this.diagnosis = new Diagnosis();
  }

  newDiagnosis(): void {
    this.submitted = false;
    this.diagnosis = new Diagnosis();
  }
  
}
