import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchingPatientComponent } from './searching-patient.component';

describe('SearchingPatientComponent', () => {
  let component: SearchingPatientComponent;
  let fixture: ComponentFixture<SearchingPatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchingPatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchingPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
