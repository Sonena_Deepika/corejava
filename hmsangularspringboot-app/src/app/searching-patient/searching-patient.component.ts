import { Component, OnInit } from '@angular/core';
import { Patient } from '../patient';
import { PatientServiceService } from '../patient-service.service';

@Component({
  selector: 'app-searching-patient',
  templateUrl: './searching-patient.component.html',
  styleUrls: ['./searching-patient.component.css']
})
export class SearchingPatientComponent implements OnInit {

  patients:Patient[]
  constructor(private patientService:PatientServiceService) { }

  ngOnInit() {
  }

  diseaseName : string;

  onSubmit(){
    this.searchPatients();
  }

  submitted=false;
  searchPatients() {
    this.patients = [];
    this.patientService.getPatientsByDiseaseName(this.diseaseName)
      .subscribe(
        data => {
          this.patients=data;
          this.submitted = true;
        },
      );
  }

}
