import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PatientServiceService {

  constructor(private http:HttpClient) { 

  }

  private baseUrl = 'http://localhost:8093/api/patients';
 
createPatient(patient: any): Observable<any> {
    return this.http.post(this.baseUrl, patient);
  }

  getPatients(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

  getPatient(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  updatePatient(patient: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}` + `/update`, patient);
   }

   deletePatient(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  getPatientsByDiseaseName(diseaseName: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/diseaseName/${diseaseName}`);
  } 

}
