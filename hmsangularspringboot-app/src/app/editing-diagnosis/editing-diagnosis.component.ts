import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Diagnosis } from '../diagnosis';
import { DiagnosisServiceService } from '../diagnosis-service.service';

@Component({
  selector: 'app-editing-diagnosis',
  templateUrl: './editing-diagnosis.component.html',
  styleUrls: ['./editing-diagnosis.component.css']
})
export class EditingDiagnosisComponent implements OnInit {

  diagnosis:Diagnosis=new Diagnosis();
  constructor(private diagnosisService:DiagnosisServiceService,private router:Router) { }

  ngOnInit() {
    this.editDiagnosis();
  }

  editDiagnosis(){
    let id=localStorage.getItem("id");
    this.diagnosisService.getDiagnosis(+id)  //convert string to number
     .subscribe(data=>{
            this.diagnosis=data;
        })
  }
  onUpdate(){
    console.log("into editing");
    this.diagnosisService.updateDiagnosis(this.diagnosis)
          .subscribe(data => {
     console.log(data);
     this.router.navigate(["diagnosis"]);
    }, error => console.log(error));
        this.diagnosis = new Diagnosis();
    }
 

}
