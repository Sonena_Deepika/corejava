import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditingDiagnosisComponent } from './editing-diagnosis.component';

describe('EditingDiagnosisComponent', () => {
  let component: EditingDiagnosisComponent;
  let fixture: ComponentFixture<EditingDiagnosisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditingDiagnosisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditingDiagnosisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
