import { Component, OnInit } from '@angular/core';
import { Physician } from '../physician';
import { PhysicianServiceService } from '../physician-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-physician',
  templateUrl: './edit-physician.component.html',
  styleUrls: ['./edit-physician.component.css']
})
export class EditPhysicianComponent implements OnInit {

  physician:Physician=new Physician();
  constructor(private physicianService:PhysicianServiceService,private router:Router) { }

  ngOnInit() {
    this.editPhysician();
  }

  editPhysician(){
    let id=localStorage.getItem("id");
    this.physicianService.getPhysician(+id)  //convert string to number
     .subscribe(data=>{
            this.physician=data;
        })
  }
  onUpdate(){
    console.log("into update");
    this.physicianService.updatePhysician(this.physician)
          .subscribe(data => {
     console.log(data);
     this.router.navigate(["physician"]);
    }, error => console.log(error));
        this.physician = new Physician();
    }
}
