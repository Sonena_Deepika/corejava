import { Component, OnInit } from '@angular/core';
import { Patient } from '../patient';
import { PatientServiceService } from '../patient-service.service';

@Component({
  selector: 'app-enroll-patient',
  templateUrl: './enroll-patient.component.html',
  styleUrls: ['./enroll-patient.component.css']
})
export class EnrollPatientComponent implements OnInit {

  patient:Patient = new Patient();
  submitted=false;
  constructor(private patientService:PatientServiceService) { 

  }

  ngOnInit() {
  }

  onSubmit(){
    console.log("successfully submitted");
    this.save();
  }

  save() {
    this.patientService.createPatient(this.patient)
      .subscribe(
        data => {
          console.log(data);
          this.submitted = true;
        },
        error => console.log(error));
    this.patient = new Patient();
  }

  newPatient(): void {
    this.submitted = false;
    this.patient = new Patient();
  }

}
