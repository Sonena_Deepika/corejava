import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddPhysicianComponent } from './add-physician/add-physician.component';
import { PhysicianListComponent } from './physician-list/physician-list.component';
import { EditPhysicianComponent } from './edit-physician/edit-physician.component';
import { HeaderComponent } from './header/header.component';
import { SearchPhysicianComponent } from './search-physician/search-physician.component';
import { EnrollPatientComponent } from './enroll-patient/enroll-patient.component';
import { PatientListComponent } from './patient-list/patient-list.component';
import { UpdatePatientComponent } from './update-patient/update-patient.component';
//import { LoginComponent } from './login/login.component';
//import { LogoutComponent } from './logout/logout.component';

import { SearchingPatientComponent } from './searching-patient/searching-patient.component';
import { CreateDiagnosisComponent } from './create-diagnosis/create-diagnosis.component';
import { DiagnosisListComponent } from './diagnosis-list/diagnosis-list.component';
import { EditingDiagnosisComponent } from './editing-diagnosis/editing-diagnosis.component';



@NgModule({
  declarations: [
    AppComponent,
    AddPhysicianComponent,
    PhysicianListComponent,
    EditPhysicianComponent,
    HeaderComponent,
    SearchPhysicianComponent,
    EnrollPatientComponent,
    PatientListComponent,
    UpdatePatientComponent,
    //LoginComponent,
    //LogoutComponent,
    SearchingPatientComponent,
    CreateDiagnosisComponent,
    DiagnosisListComponent,
    EditingDiagnosisComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
