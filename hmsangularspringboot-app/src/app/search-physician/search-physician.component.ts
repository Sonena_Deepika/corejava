import { Component, OnInit } from '@angular/core';
import { Physician } from '../physician';
import { PhysicianServiceService } from '../physician-service.service';

@Component({
  selector: 'app-search-physician',
  templateUrl: './search-physician.component.html',
  styleUrls: ['./search-physician.component.css']
})
export class SearchPhysicianComponent implements OnInit {

  physicians:Physician[]
  constructor(private physicianService:PhysicianServiceService) { }

  ngOnInit() {
  }

  department : string;

  onSubmit(){
    this.searchPhysicians();
  }

  submitted=false;
  searchPhysicians() {
    this.physicians = [];
    this.physicianService.getPhysiciansByDepartment(this.department)
      .subscribe(
        data => {
          this.physicians=data;
          this.submitted = true;
        },
      );
  }

}
