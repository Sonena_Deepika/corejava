export class Diagnosis {
    id:number;
    patientId:number;
    name:string;
    administeredBy:string;
    billAmount:number;
    paymentMode:string;
}
