import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PhysicianServiceService {

  constructor(private http:HttpClient) { 

  }

  private baseUrl = 'http://localhost:8093/api/physicians';
 
createPhysician(physician: any): Observable<any> {
    return this.http.post(this.baseUrl, physician);
  }

  getPhysicians(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

  getPhysician(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  updatePhysician(physician: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}` + `/update`, physician);
   }

   deletePhysician(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  getPhysiciansByDepartment(department: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/department/${department}`);
  }
}
