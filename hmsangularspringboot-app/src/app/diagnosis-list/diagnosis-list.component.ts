import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Diagnosis } from '../diagnosis';
import { DiagnosisServiceService } from '../diagnosis-service.service';

@Component({
  selector: 'app-diagnosis-list',
  templateUrl: './diagnosis-list.component.html',
  styleUrls: ['./diagnosis-list.component.css']
})
export class DiagnosisListComponent implements OnInit {

  diagnosiss:Observable<Diagnosis[]>
  constructor(private diagnosisService:DiagnosisServiceService,private router:Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData()
  {
    this.diagnosiss=this.diagnosisService.getDiagnosiss();
  }

  editingDiagnosis(diagnosis: Diagnosis):void {
    console.log("into editing");
    localStorage.setItem("id",diagnosis.id.toString());
    this.router.navigate(["editing"]);
  }

  deleteDiagnosis(diagnosis:Diagnosis) {
    this.diagnosisService.deleteDiagnosis(diagnosis.id)
      .subscribe(
        data => {
          console.log(data);
         this.reloadData();
        
        },
        error => console.log(error));
  }

}
