import { Component, OnInit } from '@angular/core';
import { PatientServiceService } from '../patient-service.service';
import { Router } from '@angular/router';
import { Patient } from '../patient';

@Component({
  selector: 'app-update-patient',
  templateUrl: './update-patient.component.html',
  styleUrls: ['./update-patient.component.css']
})
export class UpdatePatientComponent implements OnInit {

  patient:Patient=new Patient();
  constructor(private patientService:PatientServiceService,private router:Router) { }

  ngOnInit() {
    this.updatePatient();
  }

  updatePatient(){
    let id=localStorage.getItem("id");
    this.patientService.getPatient(+id)  //convert string to number
     .subscribe(data=>{
            this.patient=data;
        })
  }
  onUpdate(){
    console.log("into update");
    this.patientService.updatePatient(this.patient)
          .subscribe(data => {
     console.log(data);
     this.router.navigate(["patient"]);
    }, error => console.log(error));
        this.patient = new Patient();
    }

}
