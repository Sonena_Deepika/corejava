import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Physician } from '../physician';
import { PhysicianServiceService } from '../physician-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-physician-list',
  templateUrl: './physician-list.component.html',
  styleUrls: ['./physician-list.component.css']
})
export class PhysicianListComponent implements OnInit {

  physicians:Observable<Physician[]>
  constructor(private physicianService:PhysicianServiceService,private router:Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData()
  {
    this.physicians=this.physicianService.getPhysicians();
  }

  editPhysician(physician: Physician):void {
    console.log("into edit");
    localStorage.setItem("id",physician.id.toString());
    this.router.navigate(["edit"]);
  }

  deletePhysician(physician:Physician) {
    this.physicianService.deletePhysician(physician.id)
      .subscribe(
        data => {
          console.log(data);
         this.reloadData();
        
        },
        error => console.log(error));
  }

}
