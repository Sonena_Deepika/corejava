import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPhysicianComponent } from './add-physician/add-physician.component';
import { PhysicianListComponent } from './physician-list/physician-list.component';
import { EditPhysicianComponent } from './edit-physician/edit-physician.component';
import { SearchPhysicianComponent } from './search-physician/search-physician.component';
import { UpdatePatientComponent } from './update-patient/update-patient.component';
import { PatientListComponent } from './patient-list/patient-list.component';
import { EnrollPatientComponent } from './enroll-patient/enroll-patient.component';
//import { LoginComponent } from './login/login.component';
//import { LogoutComponent } from './logout/logout.component';
//import { AuthguardService } from './authguard.service';

import { SearchingPatientComponent } from './searching-patient/searching-patient.component';
import { CreateDiagnosisComponent } from './create-diagnosis/create-diagnosis.component';
import { DiagnosisListComponent } from './diagnosis-list/diagnosis-list.component';
import { EditingDiagnosisComponent } from './editing-diagnosis/editing-diagnosis.component';




const routes: Routes = [
  {
    path:'add',
    component:AddPhysicianComponent
  },
  {
    path:'physician',
    component:PhysicianListComponent
   // canActivate:[AuthguardService]
  },
  {
    path:'edit',
    component:EditPhysicianComponent
  },
  {
    path:'search',
    component:SearchPhysicianComponent
  },
  {
    path:'enroll',
    component:EnrollPatientComponent
  },
  {
    path:'patient',
    component:PatientListComponent
  },
  {
    path:'update',
    component:UpdatePatientComponent
  },
  {
    path:'searching',
    component:SearchingPatientComponent
  },
  {
    path:'create',
    component:CreateDiagnosisComponent
  },
  {
    path:'diagnosis',
    component:DiagnosisListComponent
  },
  {
    path:'editing',
    component:EditingDiagnosisComponent
  }

  /*{
    path:'login',
    component:LoginComponent
  },
  {
    path:'',
    redirectTo:'physician',
    pathMatch:'full'
  }, 
  {
    path:'logout',
    component:LogoutComponent,
    canActivate:[AuthguardService]
  }  */
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
