import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DiagnosisServiceService {

  constructor(private http:HttpClient) { 

  }

  private baseUrl = 'http://localhost:8093/api/diagnosiss';
 
createDiagnosis(diagnosis: any): Observable<any> {
    return this.http.post(this.baseUrl, diagnosis);
  }

  getDiagnosiss(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

  getDiagnosis(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  updateDiagnosis(diagnosis: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}` + `/update`, diagnosis);
   }

   deleteDiagnosis(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  
}
