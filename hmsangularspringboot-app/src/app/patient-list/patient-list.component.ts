import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Patient } from '../patient';
import { PatientServiceService } from '../patient-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {

  patients:Observable<Patient[]>
  constructor(private patientService:PatientServiceService,private router:Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData()
  {
    this.patients=this.patientService.getPatients();
  }

   updatePatient(patient: Patient):void {
    console.log("into update");
    localStorage.setItem("id",patient.id.toString());
    this.router.navigate(["update"]);
  }

  deletePatient(patient:Patient) {
    this.patientService.deletePatient(patient.id)
      .subscribe(
        data => {
          console.log(data);
         this.reloadData();
        
        },
        error => console.log(error));
  }

}
