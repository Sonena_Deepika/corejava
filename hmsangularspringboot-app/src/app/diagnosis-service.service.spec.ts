import { TestBed } from '@angular/core/testing';

import { DiagnosisServiceService } from './diagnosis-service.service';

describe('DiagnosisServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DiagnosisServiceService = TestBed.get(DiagnosisServiceService);
    expect(service).toBeTruthy();
  });
});
