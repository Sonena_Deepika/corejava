export class Patient {
    id:number;
    firstName:string;
    lastName:string;
    age:number;
    diseaseName:string;
    phone:string;
    gender:string;
}
