import { Component, OnInit } from '@angular/core';
import { Physician } from '../physician';
import { PhysicianServiceService } from '../physician-service.service';

@Component({
  selector: 'app-add-physician',
  templateUrl: './add-physician.component.html',
  styleUrls: ['./add-physician.component.css']
})
export class AddPhysicianComponent implements OnInit {

  physician:Physician = new Physician();
  submitted=false;
  constructor(private physicianService:PhysicianServiceService) { 

  }

  ngOnInit() {
  }

  onSubmit(){
    console.log("successfully submitted");
    this.save();
  }

  save() {
    this.physicianService.createPhysician(this.physician)
      .subscribe(
        data => {
          console.log(data);
          this.submitted = true;
        },
        error => console.log(error));
    this.physician = new Physician();
  }

  newCustomer(): void {
    this.submitted = false;
    this.physician = new Physician();
  }
}
